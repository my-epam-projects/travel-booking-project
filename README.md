# Travel Booking Application

## Description
This is a simple Travel Booking Application that allows users to search for flights, hotels, and rental cars. The application integrates with a third-party service, Expedia, to retrieve real-time availability and pricing information.

## Prerequisites
Before running the application, make sure you have the following installed on your system:
- Java Development Kit (JDK) 11 or later
- Maven (for building and managing dependencies)

## Getting Started
1. Clone the repository to your local machine:
    ```bash
    git clone https://github.com/your-username/travel-booking-app.git
    cd travel-booking-app
    ```

2. Build the application using Maven:
    ```bash
    mvn clean install
    ```

3. Run the application:
    ```bash
    java -jar target/travel-booking-app-1.0-SNAPSHOT.jar
    ```

4. The application will start, and you can access it at [http://localhost:8080](http://localhost:8080) in your web browser.

## Usage
### Search Flights
- Endpoint: `/api/booking/search/flights`
- Parameters:
    - `from`: Departure city
    - `to`: Destination city
    - `flyDate`: Flight departure date (format: YYYY-MM-DD)

Example:
```bash
curl -X GET "http://localhost:8080/api/booking/search/flights?from=CHIKAGO&to=NEW%20YORK&flyDate=2023-11-12"
Search Hotels
Endpoint: /api/booking/search/hotels
Parameters:
location: Hotel location
checkIn: Check-in date (format: YYYY-MM-DD)
checkOut: Check-out date (format: YYYY-MM-DD)
Example:

bash
Copy code
curl -X GET "http://localhost:8080/api/booking/search/hotels?location=LONDON&checkIn=2023-11-12&checkOut=2023-11-15"
Search Cars
Endpoint: /api/booking/search/cars
Parameters:
brand: Rental car brand
model: Rental car model
location: Pickup location
Example:

bash
Copy code
curl -X GET "http://localhost:8080/api/booking/search/cars?brand=BrandX&model=Model1&location=TOKYO"
Contributing
If you'd like to contribute to this project, please follow the standard GitHub fork and pull request workflow.

License
This project is licensed under the MIT License.

vbnet
Copy code

Replace placeholders like `your-username` and update information based on your actual


Feedback:
i took about 3 hours to complete this task with chatGPT since AI couldn't offer valid entites and controller
i designed service and controller wihthout using AI
i faces some issues with plugins since AI cannot offer valid plugins and i have sonarlight that's why i didn't add sonerQube
And AI cannot offer ExpediaService and created it with just for now and creating unit tests are awesome with AI