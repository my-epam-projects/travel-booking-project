package com.example.travelbookingproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelBookingProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelBookingProjectApplication.class, args);
    }

}
