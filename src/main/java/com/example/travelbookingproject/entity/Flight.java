package com.example.travelbookingproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Flight {

    private Long id;

    private String from;

    private String to;

    private LocalDate flyDate;

    private LocalDateTime departureTime;

    private Double price;

    public Flight(String from, String to, LocalDate flyDate, LocalDateTime departureTime, Double price) {
        this.from = from;
        this.to = to;
        this.flyDate = flyDate;
        this.departureTime = departureTime;
        this.price = price;
    }
}
