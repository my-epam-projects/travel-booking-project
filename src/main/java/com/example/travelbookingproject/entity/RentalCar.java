package com.example.travelbookingproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RentalCar {

    private Long id;

    private String brand;

    private String model;

    private Integer year;

    private String location;

    private Double pricePerDay;

    public RentalCar(String brand, String model, Integer year, String location, Double pricePerDay) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.location = location;
        this.pricePerDay = pricePerDay;
    }
}
