package com.example.travelbookingproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Hotel {


    private Long id;

    private String name;


    private String location;


    private Integer availableRooms;


    private Double pricePerNight;

    public Hotel(String name, String location, Integer availableRooms, Double pricePerNight) {
        this.name = name;
        this.location = location;
        this.availableRooms = availableRooms;
        this.pricePerNight = pricePerNight;
    }
}
