package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.Flight;
import com.example.travelbookingproject.entity.Hotel;
import com.example.travelbookingproject.entity.RentalCar;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

public interface ExpediaService {

    List<Flight> searchFlights(String from, String to, LocalDate flyDate) throws ParseException;

    List<Hotel> searchHotels(String location, LocalDate checkin, LocalDate checkout);

    List<RentalCar> searchCars(String brand, String model,String location);
}
