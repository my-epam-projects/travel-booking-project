package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.Flight;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.List;

public interface FlightService {

    ResponseEntity<List<Flight>> searchFlights(String from, String to, String flyDate) throws ParseException;
}
