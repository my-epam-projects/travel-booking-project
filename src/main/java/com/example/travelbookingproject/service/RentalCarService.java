package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.RentalCar;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface RentalCarService {
    ResponseEntity<List<RentalCar>> searchCars(String brand, String model,String location);
}
