package com.example.travelbookingproject.service.impl;

import com.example.travelbookingproject.entity.RentalCar;
import com.example.travelbookingproject.service.ExpediaService;
import com.example.travelbookingproject.service.RentalCarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RentalCarServiceImpl implements RentalCarService {

    private final ExpediaService expediaService;

    @Override
    public ResponseEntity<List<RentalCar>> searchCars(String brand, String model, String location) {
        List<RentalCar> carList = expediaService.searchCars(brand, model, location);
        if (carList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(carList);
    }
}
