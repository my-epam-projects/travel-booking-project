package com.example.travelbookingproject.service.impl;

import com.example.travelbookingproject.entity.Flight;
import com.example.travelbookingproject.entity.Hotel;
import com.example.travelbookingproject.entity.RentalCar;
import com.example.travelbookingproject.service.ExpediaService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExpediaServiceImpl implements ExpediaService {

    List<Flight> flightList = List.of(new Flight("CHIKAGO", "NEW YORK", LocalDate.of(2023, 11, 12), LocalDateTime.of(2023, 11, 12, 20, 0, 0), 300.00),
            new Flight("PARIJ", "LONDON", LocalDate.of(2023, 11, 12), LocalDateTime.of(2023, 11, 12, 20, 0, 0), 200.00),
            new Flight("TOKIO", "LONDON", LocalDate.of(2023, 12, 1), LocalDateTime.of(2023, 12, 1, 20, 0, 0), 500.00),
            new Flight("TASHKENT", "SEUL", LocalDate.of(2023, 11, 10), LocalDateTime.of(2023, 11, 10, 20, 0, 0), 250.00),
            new Flight("MADRID", "MOSKVA", LocalDate.of(2023, 12, 10), LocalDateTime.of(2023, 12, 10, 20, 0, 0), 400.00));


    List<Hotel> hotelList = List.of(new Hotel("Velere hotel", "Tashkent", 20, 500.00),
            new Hotel("Burj Al Arab Jumeirah", "United Arab Emirates", 100, 2000.00),
            new Hotel("Raffles Singapore", "Singapure", 50, 1000.00),
            new Hotel("Hilton", "Tashkent", 100, 500.00));

    List<RentalCar> carList = List.of(new RentalCar("BMW", "Model1", 2020, "LONDON", 200.00),
            new RentalCar("MERS", "Model2", 2019, "PARIJ", 300.00),
            new RentalCar("GM", "Gentra", 2020, "TASHKENT", 100.00),
            new RentalCar("BMW", "Model3", 2020, "Moskva", 200.00),
            new RentalCar("MERS", "Model4", 2019, "Tokio", 300.00),
            new RentalCar("GM", "malibu", 2020, "Andijan", 100.00));

    @Override
    public List<Flight> searchFlights(String from, String to, LocalDate flyDate) {

        List<Flight> filteredFlights = flightList.stream()
                .filter(flight ->
                        flight.getFrom().equalsIgnoreCase(from) &&
                                flight.getTo().equalsIgnoreCase(to) &&
                                flight.getFlyDate().isEqual(flyDate))
                .collect(Collectors.toList());

        return filteredFlights.isEmpty() ? new ArrayList<>() : filteredFlights;
    }

    @Override
    public List<Hotel> searchHotels(String location, LocalDate checkin, LocalDate checkout) {
        List<Hotel> availableHotels = hotelList.stream()
                .filter(hotel -> hotel.getLocation().equalsIgnoreCase(location))
                .toList();
        return availableHotels.isEmpty() ? new ArrayList<>() : availableHotels;
    }

    @Override
    public List<RentalCar> searchCars(String brand, String model, String location) {
        List<RentalCar> rentalCarList = carList.stream()
                .filter(car -> car.getBrand().equalsIgnoreCase(brand) &&
                        car.getModel().equalsIgnoreCase(model) &&
                        car.getLocation().equalsIgnoreCase(location))
                .toList();
        return rentalCarList.isEmpty() ? new ArrayList<>() : rentalCarList;
    }
}
