package com.example.travelbookingproject.service.impl;

import com.example.travelbookingproject.entity.Hotel;
import com.example.travelbookingproject.service.ExpediaService;
import com.example.travelbookingproject.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final ExpediaService expediaService;

    @Override
    public ResponseEntity<List<Hotel>> searchHotels(String location, String checkIn, String checkOut) {
        List<Hotel> hotelList = expediaService.searchHotels(location, LocalDate.parse(checkIn), LocalDate.parse(checkOut));
        if (hotelList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(hotelList);
    }
}
