package com.example.travelbookingproject.service.impl;

import com.example.travelbookingproject.entity.Flight;
import com.example.travelbookingproject.service.ExpediaService;
import com.example.travelbookingproject.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    private final ExpediaService expediaService;

    @Override
    public ResponseEntity<List<Flight>> searchFlights(String from, String to, String flyDate) throws ParseException {
        List<Flight> flights = expediaService.searchFlights(from, to, LocalDate.parse(flyDate));
        if (flights.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(flights);
    }
}
