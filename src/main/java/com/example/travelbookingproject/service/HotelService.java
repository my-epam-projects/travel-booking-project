package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.Hotel;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface HotelService {
    ResponseEntity<List<Hotel>> searchHotels(String location, String checkIn, String checkOut);

}
