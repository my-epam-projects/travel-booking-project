package com.example.travelbookingproject.controller;

import com.example.travelbookingproject.entity.Flight;
import com.example.travelbookingproject.entity.Hotel;
import com.example.travelbookingproject.entity.RentalCar;
import com.example.travelbookingproject.service.FlightService;
import com.example.travelbookingproject.service.HotelService;
import com.example.travelbookingproject.service.RentalCarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/booking")
public class BookingController {


    private final FlightService flightService;
    private final HotelService hotelService;
    private final RentalCarService rentalCarService;

    @GetMapping("/search/flights")
    public ResponseEntity<List<Flight>> searchFlights(
            @RequestParam String from,
            @RequestParam String to,
            @RequestParam String flyDate) throws ParseException {
        return flightService.searchFlights(from, to,flyDate);
    }

    @GetMapping("/search/hotels")
    public ResponseEntity<List<Hotel>> searchHotels(@RequestParam String location,
                                                    @RequestParam String checkIn,
                                                    @RequestParam String checkOut){
        return hotelService.searchHotels(location,checkIn,checkOut);
    }

    @GetMapping("/search/cars")
    public ResponseEntity<List<RentalCar>> searchCars(@RequestParam String brand,
                                                      @RequestParam String model,
                                                      @RequestParam String location){
        return rentalCarService.searchCars(brand,model,location);
    }
}
