package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.Flight;
import com.example.travelbookingproject.service.impl.FlightServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class FlightServiceTest {

    @Mock
    private ExpediaService expediaService;

    @InjectMocks
    private FlightServiceImpl flightService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSearchFlightsFound() throws ParseException {
        // Arrange
        String from = "CHIKAGO";
        String to = "NEW YORK";
        String flyDate = "2023-11-12";

        List<Flight> mockFlights = Collections.singletonList(new Flight(1L, "CHIKAGO", "NEW YORK", LocalDate.parse("2023-11-12"), null, 300.00));
        when(expediaService.searchFlights(from, to, LocalDate.parse(flyDate))).thenReturn(mockFlights);

        // Act
        ResponseEntity<List<Flight>> response = flightService.searchFlights(from, to, flyDate);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockFlights, response.getBody());
    }

    @Test
    void testSearchFlightsNotFound() throws ParseException {
        // Arrange
        String from = "CHIKAGO";
        String to = "NEW YORK";
        String flyDate = "2023-11-12";

        when(expediaService.searchFlights(from, to, LocalDate.parse(flyDate))).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<List<Flight>> response = flightService.searchFlights(from, to, flyDate);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
