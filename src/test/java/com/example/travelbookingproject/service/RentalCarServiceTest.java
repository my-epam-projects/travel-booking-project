package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.RentalCar;
import com.example.travelbookingproject.service.impl.RentalCarServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

 class RentalCarServiceTest {

    @Mock
    private ExpediaService expediaService;

    @InjectMocks
    private RentalCarServiceImpl rentalCarService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSearchCarsFound() {
        // Arrange
        String brand = "BrandX";
        String model = "Model1";
        String location = "TOKYO";

        List<RentalCar> mockCars = Collections.singletonList(new RentalCar(1L, "BrandX", "Model1", 2023, "TOKYO", 50.00));
        when(expediaService.searchCars(brand, model, location)).thenReturn(mockCars);

        // Act
        ResponseEntity<List<RentalCar>> response = rentalCarService.searchCars(brand, model, location);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockCars, response.getBody());
    }

    @Test
    void testSearchCarsNotFound() {
        // Arrange
        String brand = "BrandX";
        String model = "Model1";
        String location = "TOKYO";

        when(expediaService.searchCars(brand, model, location)).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<List<RentalCar>> response = rentalCarService.searchCars(brand, model, location);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
