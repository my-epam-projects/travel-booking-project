package com.example.travelbookingproject.service;

import com.example.travelbookingproject.entity.Hotel;
import com.example.travelbookingproject.service.impl.HotelServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


class HotelServiceTest {

    @Mock
    private ExpediaService expediaService;

    @InjectMocks
    private HotelServiceImpl hotelService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSearchHotelsFound() {
        // Arrange
        String location = "LONDON";
        String checkIn = "2023-11-12";
        String checkOut = "2023-11-15";

        List<Hotel> mockHotels = Collections.singletonList(new Hotel(1L, "HotelA", "LONDON", 100, 200.00));
        when(expediaService.searchHotels(location, LocalDate.parse(checkIn), LocalDate.parse(checkOut))).thenReturn(mockHotels);

        // Act
        ResponseEntity<List<Hotel>> response = hotelService.searchHotels(location, checkIn, checkOut);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockHotels, response.getBody());
    }

    @Test
    void testSearchHotelsNotFound() {
        // Arrange
        String location = "LONDON";
        String checkIn = "2023-11-12";
        String checkOut = "2023-11-15";

        when(expediaService.searchHotels(location, LocalDate.parse(checkIn), LocalDate.parse(checkOut))).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<List<Hotel>> response = hotelService.searchHotels(location, checkIn, checkOut);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
