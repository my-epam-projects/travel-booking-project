package com.example.travelbookingproject.controller;

import com.example.travelbookingproject.entity.Flight;
import com.example.travelbookingproject.entity.Hotel;
import com.example.travelbookingproject.entity.RentalCar;
import com.example.travelbookingproject.service.FlightService;
import com.example.travelbookingproject.service.HotelService;
import com.example.travelbookingproject.service.RentalCarService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
 class BookingControllerTest {


    @Mock
    private FlightService flightService;

    @Mock
    private HotelService hotelService;

    @Mock
    private RentalCarService rentalCarService;

    @InjectMocks
    private BookingController bookingController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(bookingController).build();
    }

    @Test
    void testSearchFlights() throws Exception {
        // Arrange
        List<Flight> mockFlights = Collections.singletonList(new Flight(1L, "CHIKAGO", "NEW YORK", LocalDate.now(), LocalDateTime.now(), 300.00));
        when(flightService.searchFlights(anyString(), anyString(), anyString())).thenReturn(ResponseEntity.ok(mockFlights));

        // Act & Assert
        mockMvc.perform(get("/api/booking/search/flights")
                        .param("from", "CHIKAGO")
                        .param("to", "NEW YORK")
                        .param("flyDate", "2023-11-12"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].from").value("CHIKAGO"))
                .andExpect(jsonPath("$[0].to").value("NEW YORK"))
                .andExpect(jsonPath("$[0].price").value(300.00));
    }

    @Test
    void testSearchHotels() throws Exception {
        // Arrange
        List<Hotel> mockHotels = Collections.singletonList(new Hotel("Hilton", "Tashkent", 20, 200.00));
        when(hotelService.searchHotels(anyString(), anyString(), anyString())).thenReturn(ResponseEntity.ok(mockHotels));

        // Act & Assert
        mockMvc.perform(get("/api/booking/search/hotels")
                .param("location", "Tashkent")
                .param("checkIn", "2023-11-12")
                .param("checkOut", "2023-11-15"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value("Hilton"))
                .andExpect(jsonPath("$[0].location").value("Tashkent"))
                .andExpect(jsonPath("$[0].pricePerNight").value(200.00));
    }

    @Test
    void testSearchCars() throws Exception {
        // Arrange
        List<RentalCar> mockCars = Collections.singletonList(new RentalCar("BrandX", "Model1",2000, "TOKYO", 50.00));
        when(rentalCarService.searchCars(anyString(), anyString(), anyString())).thenReturn(ResponseEntity.ok(mockCars));

        // Act & Assert
        mockMvc.perform(get("/api/booking/search/cars")
                        .param("brand", "BrandX")
                        .param("model", "Model1")
                        .param("location", "TOKYO"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].brand").value("BrandX"))
                .andExpect(jsonPath("$[0].model").value("Model1"))
                .andExpect(jsonPath("$[0].location").value("TOKYO"))
                .andExpect(jsonPath("$[0].pricePerDay").value(50.00));
    }
}

